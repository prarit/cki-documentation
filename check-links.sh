#!/bin/bash

set -euo pipefail

hugo server &
trap 'trap - EXIT && kill -- -$$' EXIT
sleep 10

# ignore
# - legacy favicons
# - GitLab URLs requiring login
# - GitLab links to markdown anchors that cannot be resolved as markdown is rendered dynamically
# - Red Hat internal websites
muffet http://localhost:1313/ \
    -t 30 \
    -e '.*favicon.ico$' \
    -e 'https://applecrumble.internal.cki-project.org/.*' \
    -e 'https://brewweb.engineering.redhat.com/brew/.*' \
    -e 'https://datagrepper.engineering.redhat.com/.*' \
    -e 'https://documentation.internal.cki-project.org/.*' \
    -e 'https://github.com/kubernetes/community/blob/master/contributors/design-proposals/node/resource-qos.md#requests-and-limits' \
    -e 'https://gitlab.cee.redhat.com/.*' \
    -e 'https://gitlab.com/cki-project/.*#.*' \
    -e 'https://gitlab.com/cki-project/documentation/-/commit/.*' \
    -e 'https://gitlab.com/cki-project/documentation/-/edit/.*' \
    -e 'https://gitlab.com/cki-project/documentation/-/forks/new' \
    -e 'https://gitlab.com/cki-project/documentation/-/issues/new\?.*' \
    -e 'https://gitlab.com/groups/cki-project/-/edit' \
    -e 'https://gitlab.com/projects/new\?namespace_id=3970123' \
    -e 'https://gitlab.com/redhat/red-hat-ci-tools/.*' \
    -e 'https://gitlab.corp.redhat.com/.*' \
    -e 'https://helpdesk.redhat.com' \
    -e 'https://listman.redhat.com/mailman/private/kernel-info/' \
    -e 'https://one.redhat.com/.*' \
    -e 'https://projects.engineering.redhat.com/.*' \
    -e 'https://red.ht/GitLabSSO' \
    -e 'https://rover.redhat.com/.*' \
    -e 'https://source.redhat.com/.*'

# if no dangling links were found, shut down normally
kill %1
trap - EXIT
