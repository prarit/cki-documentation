#!/bin/bash

set -euo pipefail

# shellcheck disable=SC1091
. cki_utils.sh

cki_update_repo https://gitlab.com/cki-project/inventory

mkdir -p data/inventory
for i in inventory/*/; do
    link_target=data/inventory/$(basename "${i}")
    if ! [ -h "${link_target}" ]; then
        ln -s "../../${i}" "${link_target}"
    fi
done

for i in data/inventory/*/*; do
    j=${i#data/inventory/}
    j=${j%.yml}
    fn="content/docs/hacking/inventory/${j}.md"
    if ! [ -f "${fn}" ]; then
        echo "Creating stub page ${fn}"
        mkdir -p "${fn%/*}"
        echo -e "---\ntitle: \"${j##*/}\"\nlayout: inventory-details\n---" > "${fn}"
    fi
done
