---
title: Targeted tests for merge requests
description: Tests that are run when specific kernel source files are updated.
linkTitle: Targeted tests
weight: 45
---

Targeted testing is the term used to refer to CKI tests that are configured to run
only when specific kernel source files are updated in a merge request.

**In the future merge requests with missing targeted tests will be blocked.**

## Configuration

The configuration is done in the test case in [kpet-db].

Example: [block tests]

```yaml
...
sources:
or:
- block/blk-cgroup.c
- block/blk-mq-debugfs.c
- block/elevator.c
- block/blk-mq-sched.c
- block/blk-mq.c
- block/blk-mq-sysfs.c
- block/cfq-iosched.c
- block/kyber-iosched.c
- block/bfq-iosched.c
- block/blk-core.c
- block/mq-deadline.c
...
```

## Missing targeted tests

Merge requests that have missing targeted tests have the
'TargetedTestingMissing' label.

### What that means?

It means that no file updated by the merge request has a specific test
that covers it.

### What to do?

1. Looking at [CKI tests], maybe some test could be configured to run on that file.
It can be confirmed by running the test using code coverage.

2. Write/onboard new test case for it.

3. Talk with QE that covers this area to write/onboard new test case for it.

Don't hesitate to contact CKI team for help.

[kpet-db]: https://gitlab.com/cki-project/kpet-db/
[block tests]: https://gitlab.com/cki-project/kpet-db/-/blob/main/cases/storage/block/index.yaml
[CKI tests]: https://gitlab.com/cki-project/kernel-tests/
