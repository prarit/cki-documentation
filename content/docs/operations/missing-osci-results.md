---
title: Fixing missing OSCI results
linkTitle: Missing OSCI results
description: How to investigate a report of a missing OSCI results
---

## Problem

You get a report of missing OSCI results, e.g.

```plain
Any idea why CKI results don't appear on OSCI dashboard?
https://dashboard.osci.redhat.com/#/artifact/brew-build/aid/35242921
```

## Steps

1. From the OSCI dashboard, determine the Brew taskID.

2. Check the brew trigger logs in Grafana via [`{deployment="brew-trigger"} |=
   "12345"`]. Change the filter expression to a suitable part of the time stamp
   to see the context of the messages and whether a pipeline was actually
   triggered and or any errors occurred.

3. Check the pipeline URL obtained from the logs for any problems.

4. Check whether `osci` is included in the `report_types` [trigger variable].
   This can be either done from the commit message or via the `Variables API`
   link included in the job output.

5. Check the UMB messenger logs in Grafana for messages related to the pipeline
   ID via [`{deployment="umb-messenger"} |= "12345"`]. If there are any error
   messages, continue with [investigating UMB problems].

[`{deployment="brew-trigger"} |= "12345"`]: https://applecrumble.internal.cki-project.org/explore?orgId=1&left=%5B%22now-1d%22,%22now%22,%22Loki%22,%7B%22expr%22:%22%7Bdeployment%3D%5C%22brew-trigger%5C%22%7D%7C%3D%5C%2212345%5C%22%22%7D%5D
[trigger variable]: ../user_docs/configuration.md
[`{deployment="umb-messenger"} |= "12345"`]: https://applecrumble.internal.cki-project.org/explore?orgId=1&left=%5B%22now-1d%22,%22now%22,%22Loki%22,%7B%22expr%22:%22%7Bdeployment%3D%5C%22umb-messenger%5C%22%7D%20%7C%3D%20%5C%2212345%5C%22%22%7D%5D
[investigating UMB problems]: umb-problems.md
