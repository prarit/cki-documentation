---
title: Updating y-stream composes
linkTitle: Updating y-stream composes
description: How to update y-zstream composes
---

## Problem

CKI uses manually selected nightly composes when testing patches or builds for
y-stream kernels. These composes are periodically removed from Beaker. In
general, composes in [kpet-db] are updated once per week. This update process
is needed to make sure no new issues are introduced, or at least add those new
issues as known issues to [DataWarehouse] so they don't block builds/MRs.

## Steps

1. Search in Beaker for the latest y-stream compose for the major release.
   Example for rhel8 (currently the y-stream builds are based on RHEL-8.6):

   ```shell
   bkr distros-list --name=RHEL-8.6% --limit=1 --tag CTS_NIGHTLY

1. Find the corresponding BUILDROOT.

   ```shell
   curl -s \
     "https://cts.engineering.redhat.com/api/1/composes/${nightly_compose}" | \
     jq -r .children[] | grep BUILDROOT

1. Update the values in the [kpet-db] tree file, in this case, `tree/rhel8.z`.

1. Create a merge request for the [kpet-db] repository.

1. Trigger the CKI CI bot.

   ```plain
   @cki-ci-bot, please test [skip_beaker=false] [baseline/rhel8] [skip_results=true]

   > **NOTE:** `skip_results=true` is used as this pipeline doesn't publish
   results to [DataWarehouse].

1. Check the test executions. If there are new non-blocking failures, they
   should be added to the [DataWarehouse] with a proper ticket to track them.
   For blocking failures, a [ServiceNow] ticket for the Beaker team has to be
   filed. The current compose needs to be kept until a new compose is available
   where those failures are resolved.

[kpet-db]: https://gitlab.com/cki-project/kpet-db
[DataWarehouse]:https://datawarehouse.cki-project.org/
[ServiceNow]: https://redhat.service-now.com/help
