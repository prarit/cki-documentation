---
title: Triggering test pipelines using the CKI bot
linkTitle: Triggering test pipelines with CKI bot
description: Features and details of CKI bot configuration
---

It is possible to trigger testing pipelines on merge requests in selected
projects under the [CKI group]. These pipelines can be used to test new proposed
changes, as well as rerun the testing to e.g. reproduce bugs.

The merge requests eligible for testing get a comment by the [CKI CI Bot] that
outlines the basics of how to trigger the testing.

The code of the bot lives in the [cki-tools] project. This file also contains
the text of the welcome message the bot posts on the merge requests. The
[configuration file] itself is available in the deployment-all project.

> **NOTE**: This document is an *extension* of the information provided in the
> comment on the merge requests by the bot and it is not meant as a substitute
> to the information in the comments. Consult the comments for common questions
> and project specific settings first, and consider extending the comment with
> information you find beneficial!

## Default settings

Each project has a different set of pipelines that will get triggered if no
specific pipelines are specified (e.g. when using a generic `@cki-ci-bot test`
command). The default pipeline groups can be found in the aforementioned
[configuration file] under the `default_pipelines` keys of the `project`
settings, and the default branches for each pipeline type can be found at the
same place under the `pipelines` settings under `default_branches`.

The created test pipelines have a `retrigger=true` trigger variable added, which
makes the pipelines ignored by the rest of the CKI infrastructure (e.g. pipeline
herder or reporting). Beaker testing is disabled as well to conserve resources.

## Configuring triggered pipelines

Environment/trigger variables can be passed to the triggered pipelines by using
the `[key=value]` syntax. The variables are passed to *every* pipeline triggered
by the single comment, thus if you wish to trigger different pipelines with
different variables passed to them you have to post multiple comments to the
bot.

*Any* new variable can be passed to the pipeline - the variables passed to the
bot are not limited to the [user facing variables] nor the [pipeline specified
defaults].

### Default variables and overrides

For some projects, specific variables are added or overridden on the background
by the bot directly so the developer doesn't have to override them in the
comment. The variable additions and overrides try to mimic useful defaults for
each specific project, e.g. adjusting the project installation URL to the one
with the proposed changes, or changing the container tags to the ones built in
the containers merge request.

The default variables for each project can be found in the `projects` settings
in the [configuration file] under `variables` key and the overrides under the
`override` key.

#### Overriding package URLs

For any projects defined in `GITLAB_COM_PACKAGES` and `GITLAB_COM_DATA_PROJECTS`
pipeline variables (check the current values in the [pipeline specified defaults]),
an URL override can be passed to the bot in the format

```text
<PROJECT>_pip_url = git+https://url.com/<USER>/<PROJECT>@<BRANCH>
```

where `<PROJECT>` *in the variable name* is the project name with any `-`
replaced by `_`.

This override is automatically added by the bot if the MR being tested belongs
to the `<PROJECT>`. Adding extra overrides provides a way to test changes from
multiple projects in a single MR together.

As an example, the following variable will test with the `upt` version from
merge request 196:

```text
upt_pip_url = git+https://gitlab.com/cki-project/upt.git/@refs/merge-requests/196/merge
```

The following variables are available:

| Variable                                  | Example                                                                                                                   |
|-------------------------------------------|---------------------------------------------------------------------------------------------------------------------------|
| `pipeline_definition_branch_override`     | `e0e65056b00844f90c027aeffb3b4036738aeee5`                                                                                |
| `pipeline_definition_repository_override` | `https://gitlab.com/cki-project/pipeline-definition`                                                                      |
| `datadefinition_pip_url`                  | `git+https://gitlab.com/cki-project/datadefinition.git/@refs/merge-requests/1234/merge`                                   |
| `cki_lib_pip_url`                         | `git+https://gitlab.com/cki-project/cki_lib.git/@refs/merge-requests/1234/merge`                                          |
| `cki_tools_pip_url`                       | `git+https://gitlab.com/cki-project/cki_tools.git/@refs/merge-requests/1234/merge`                                        |
| `reporter_pip_url`                        | `git+https://gitlab.com/cki-project/reporter.git/@refs/merge-requests/1234/merge`                                         |
| `skt_pip_url`                             | `git+https://gitlab.com/cki-project/skt.git/@refs/merge-requests/1234/merge`                                              |
| `upt_pip_url`                             | `git+https://gitlab.com/cki-project/upt.git/@refs/merge-requests/1234/merge`                                              |
| `kpet_pip_url`                            | `git+https://gitlab.com/cki-project/kpet.git/@refs/merge-requests/1234/merge`                                             |
| `kpet_db_pip_url`                         | `git+https://gitlab.com/cki-project/kpet-db.git/@refs/merge-requests/1234/merge`                                          |
| `suites_zip_url`                          | `https://gitlab.com/api/v4/projects/cki-project%2fkernel-tests/repository/archive.zip?sha=refs/merge-requests/1234/merge` |

#### `tests_only=true` setting

In an effort to speed up test pipeline runs, projects related to the testing
stage are configured with the `tests_only=true` variable. This variable skips
the kernel builds (or downloads, in case of Koji/Brew pipelines) and reuses
the artifacts from the original retriggered pipeline. Only `setup` and later
stages run in these pipelines.

> **NOTE**: The pipelines triggered with `tests_only=true` variable will fail if
> the artifacts of the retriggered pipelines already expired!

You can specify `tests_only=false` to the pipelines if your run requires
regenerating the artifacts or kernel builds.

## Pipeline group mappings

All information about where the pipeline groups get triggered are available in
the [configuration file] under the `pipelines` settings. For example, the
`upstream` and `koji` pipeline groups trigger pipelines in specific branches
of the [trusted-contributors] pipeline project.

### Triggering non-default pipelines

The pipeline groups expose the pipelines in the whole project, even if only
selected branches are set up to be triggered by default. However, since the
whole project is exposed, any pipeline from that project can be retriggered
by passing a pipeline ID to the bot together with the group, e.g.
`[upstream/123456]` (where `123456` is the ID of the pipeline we wish to rerun).

As multiple groups can refer to the same pipeline project, there may be multiple
ways to retrigger the same pipeline. Using the above example, we could also use
`[koji/123456]` to retrigger the same pipeline, as the `koji` group also refers
to the [trusted-contributors] pipeline project.

## Examples

```text
@cki-ci-bot test
```

Run all default pipelines, with default settings.

```text
@cki-ci-bot test [brew]
```

Run all default pipelines for a specific pipeline group, in this case `brew`.

```text
@cki-ci-bot test [baseline/123456] [kpet_pip_url=https://test.url/name/kpet@mybranch]
```

Run pipeline `123456` from the pipeline project exposed by the `baseline` group,
and use a new test version of the `kpet` project to test it.

```text
@cki-ci-bot test [baseline/123456] [kpet_pip_url=https://test.url/name/kpet@mybranch]
[tests_only=true]
```

Same as before, but skip building the kernel and reuse artifacts from the
original pipeline to save time.

```text
@cki-ci-bot test [baseline/123456] [patch_urls=https://patch.url/1 https://patch.url/2]
```

Run pipeline `123456` from the pipeline project exposed by the `baseline` group,
but apply linked kernel patches before building the kernel, e.g. to test fixes
provided by the developers.

[CKI group]: https://gitlab.com/cki-project
[CKI CI Bot]: https://gitlab.com/cki-ci-bot
[cki-tools]: https://gitlab.com/cki-project/cki-tools/-/blob/main/cki/cki_tools/gitlab_ci_bot.py
[configuration file]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/tree/main/openshift/gitlab-ci-bot/10-configmap.yml.j2.d/config.yml
[user facing variables]: ../user_docs/configuration.md
[pipeline specified defaults]: https://gitlab.com/cki-project/pipeline-definition/-/blob/main/pipeline/variables.yml
[trusted contributors]: https://gitlab.com/redhat/red-hat-ci-tools/kernel/cki-internal-pipelines/cki-trusted-contributors
