---
title: Zstreaming a kernel release
description: Steps to do when a kernel stream is to be released.
---

## Background

Every once in a while, a RHEL stream is released. As CKI needs to run with
matching environment, we need to update our setup to match this, in an order
that doesn't break the runs in between.

## Timeline and requirements

There is no hard timeline on when these changes need to be put in place. The
only requirement to start the process is to have the kernel zstream branch
created by the maintainers. This happens pretty early, and is announced on
[kernel-info]. Of course, the steps requiring the ystream compose and zstream
containers need to wait for their availability.

A good indicator to start the process is the kernel maintainers changing the
kernel makefile configuration to officially tag the new zstream. This usually
happens around the RC release of the new zstream compose.

Package dependencies and conflicts between the new zstream and ystream may
increase the priority of the changes on CKI side. This can happen e.g. when the
new ystream kernel or the tests start requiring package versions only available
in the new ystream compose. If this is the case, kernel or test maintainers
will point it out, or we'll notice the new pipelines consistently failing.

## Steps to take

1. Add the zstream branch to the [gitlab-ci-bot config].
1. Create a zstream tree in [kpet-db]. Also add the realtime tree if realtime
   kernels are supported for this stream. The tree(s) should match the ystream
   tree for now.
1. Update the [kpet tree selection script] to handle new NVR range or disttag
   match.
1. **Only needed for RHEL8 and newer**. Submit an MR to the kernel repository
   to add new `disttag_override` value to all pipelines in the new zstream
   branch. Check the MR pipeline picks the new zstream kpet tree. Sometimes,
   the kernel maintainers submit this change together with changing the kernel
   makefiles and it only needs to be reviewed.
1. Update the ystream tree file in [kpet-db] to point to the new ystream
   compose.
1. Once the RHEL zstream is GA, a new UBI container should become available.
   Create a new [builder container] with it. You should be able to copy the
   definition of an older zstream container and only override the stream value.
   Call the CKI bot in the containers MR with the `builder_image` override of
   this new container to test the building *and* booting of the kernel works
   well.
1. If released containers are used for the zstream pipeline, tag and release
   a new version. Follow the steps to [tag and release new pipeline images] to
   do this.
1. Create another MR to the kernel repository for the zstream branch to switch
   the `builder_image` to the newly created container.

[kernel-info]: https://listman.redhat.com/mailman/private/kernel-info/
[kpet-db]: https://gitlab.com/cki-project/kpet-db/-/tree/main/trees
[kpet tree selection script]: https://gitlab.com/cki-project/cki-tools/-/blob/main/cki/cki_tools/select_kpet_tree.py
[builder container]: https://gitlab.com/cki-project/containers/-/tree/main/builds
[tag and release new pipeline images]: https://cki-project.org/docs/hacking/background/container-images/#deploying-in-the-pipeline
[gitlab-ci-bot config]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/openshift/gitlab-ci-bot/10-configmap.yml.j2.d/config.yml
