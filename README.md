# CKI documentation website

The source code for [cki-project.org].

It uses [Hugo] with a the [Docsy] theme.

## Documentation

The markdown source for the CKI documentation can be found in the
[content/docs] directory.

## Development

You have to do three steps to start writing new documentation:

- Clone all submodules *recursively*
- Update inventory
- Install cki-lib

### Developing directly on the host system

This option is not the recommended, but you can use it.

Make sure to clone all submodules *recursively*, update the inventory and install 
[cki-lib] by running

```bash
git submodule update --init --recursive
npm i --no-package-lock
pip install -r requirements.txt # you should use virtualenv
./update-inventory.sh
```

To compile the site and serve it, simply type `hugo server` and then visit
<http://localhost:1313> with your browser. It's very convenient when developing
the site because it will compile any changes you make and reload the page
automatically in your browser.

The actual compiled site, if you need the files, will be under `public/`.

### Development using container images

It's possible to use containers for local development, or test the site,
without the need to install `hugo` or its dependencies.

The first step is always to be sure the submodules are cloned and the
inventory updated as explained above.

#### Run the site locally

Do the same steps via podman:

```bash
podman run \
       --rm \
       -it \
       -w /code \
       -v .:/code:z \
       -p 1313:1313 \
       registry.gitlab.com/cki-project/cki-tools/cki-tools:latest \
       bash -c "git submodule update --init --recursive \
                && ./update-inventory.sh \
                && hugo server --bind '0.0.0.0'"
```

**NOTE**: It can run with [Docker] instead of [Podman]. Just replace `podman`
with `docker`.

Now the documentation site will be available at <http://localhost:1313> and
any change made to the local files will be shown in your browser.

#### Lint the Markdown

It's important to have correct Markdown format, so every time
a change is pushed to the repo it gets check for errors.

Check locally for possible errors using the container image:

```bash
podman run \
       --rm \
       -it \
       -w /code \
       -v .:/code:z \
       registry.gitlab.com/cki-project/cki-tools/cki-tools:latest \
       markdownlint content/
```

#### Check for broken links

It's also important to avoid broken links, so every time
a change is pushed to the repo it gets checked for broken links.

Check locally for possible broken links using the container image:

```bash
podman run \
       --rm \
       -it \
       -w /code \
       -v .:/code:z \
       registry.gitlab.com/cki-project/cki-tools/cki-tools:latest \
       ./check-links.sh
```

[Docsy]: https://www.docsy.dev/
[content/docs]: content/docs
[Docker]: https://docs.docker.com
[Podman]: https://podman.io
[cki-lib]: https://gitlab.com/cki-project/cki-lib
[cki-project.org]: https://cki-project.org/
[Hugo]: https://gohugo.io/
